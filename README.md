# Cloudflare Updater

So I don't forget the settings...

This is made to be run as a single-fire service.

## Windows

In Task Scheduler, create a new (not basic) task.
The following sections are tab names.

### General

- Name: Whatever you feel like.
- Description: Optional.
- Choose "Run whether user is logged on or not".
- Check "Do not store password. ...".
- Check "Hidden".

### Triggers

- Choose to "Begin the task: At startup".
- Check "Repeat task every:" and choose "5 minutes" and set "for a duration of:" to "Indefinitely".

### Actions

- Set "Atcion:" to "Start a program".
- In "Program/script:" enter the path (or browse) to your python executable.
- "Add arguments:" should contain the full path to the "main.py" of this project, just in case.
- "Start in:" should also be set to the full path of this project's directory just in case.

## Linux

TODO at some point for systemd service unit files...

