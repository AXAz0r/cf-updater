import abc
import errno
import os.path

import yaml


class CFUConfig(abc.ABC):
    def __init__(self):
        super().__init__()
        print('Loading config...')
        if os.path.exists('config/core.yml'):
            with open('config/core.yml') as core_config_file:
                self.raw = yaml.safe_load(core_config_file)
            self.token = self.raw.get('api_token')
            self.zone = self.raw.get('zone_id')
            self.account = self.raw.get('account_id')
            self.record = self.raw.get('record_name')
            self.proxied = self.raw.get('proxied')
            self.validate()
        else:
            print('Missing config file.')
            exit(errno.ENODATA)

    def validate(self) -> bool:
        types = {
            'token': str,
            'zone': str,
            'account': str,
            'record': str,
            'prodxied': bool
        }
        valid = True
        for key in types.keys():
            needed = types.get(key)
            current = getattr(self, key)
            if not isinstance(needed, type(current)):
                valid = False
                break
        return valid
