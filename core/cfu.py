import abc
import json
from typing import Optional

import requests

from core.config import CFUConfig

CF_API_BASE = 'https://api.cloudflare.com/client/v4'
CF_USER_VERIFY = f'{CF_API_BASE}/user/tokens/verify'
CF_ZONE_API = f'{CF_API_BASE}/zones'


class CloudFlareUpdater(abc.ABC):
    def __init__(self):
        super().__init__()
        self.cfg = CFUConfig()

    def headers(self) -> dict:
        return {
            'Authorization': f'Bearer {self.cfg.token}',
            'Content-Type': 'application/json'
        }

    def data(self, ip: str) -> dict:
        return {
            'type': 'A',
            'name': self.cfg.record,
            'content': ip,
            'ttl': 1,
            'proxied': self.cfg.proxied
        }

    @staticmethod
    def explain(data: dict):
        for error in data.get('errors'):
            print(f'{error.get("code")}: {error.get("message")}')
        for message in data.get('messages'):
            print(message)

    def verify(self) -> bool:
        print('Verifying details...')
        data = json.loads(requests.get(CF_USER_VERIFY, headers=self.headers()).text)
        success = data.get('success')
        return success

    @staticmethod
    def get_ip() -> str:
        data = json.loads(requests.get('https://api.my-ip.io/ip.json').text)
        return data.get('ip')

    def get_zone(self) -> Optional[dict]:
        print('Retrieving zone...')
        zone = None
        data = json.loads(requests.get(f'{CF_ZONE_API}/{self.cfg.zone}/dns_records', headers=self.headers()).text)
        for result in data.get('result'):
            if result.get('name') == self.cfg.record:
                zone = result
                break
        return zone

    def create_zone(self):
        print('Creating a new zone...')
        ip = self.get_ip()
        data = json.loads(
            requests.post(
                f'{CF_ZONE_API}/{self.cfg.zone}/dns_records',
                headers=self.headers(),
                json=self.data(ip)
            ).text
        )
        if not data.get('success'):
            print('Creating a zone failed!')
            self.explain(data)

    def update_zone(self, record: dict):
        print('Updating existing zone...')
        ip = self.get_ip()
        if ip != record.get('content'):
            data = json.loads(
                requests.put(
                    f'{CF_ZONE_API}/{self.cfg.zone}/dns_records/{record.get("id")}',
                    headers=self.headers(),
                    json=self.data(ip)
                ).text
            )
            if not data.get('success'):
                print('Creating a zone failed!')
                self.explain(data)
        else:
            print('The IP is up to date.')

    def update(self):
        record = self.get_zone()
        if record:
            self.update_zone(record)
        else:
            self.create_zone()

    def run(self):
        print('Starting updater...')
        if self.verify():
            print('Verification successful.')
            self.update()
        else:
            print('Verification failed.')
        print('Done.')
