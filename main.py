from core.cfu import CloudFlareUpdater


def main():
    core = CloudFlareUpdater()
    core.run()


if __name__ == '__main__':
    main()
